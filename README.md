# Europeana full text resource aggregator

CLARIN style docker image wrapping the scripts defined in the
[europeana-fulltext-aggregations](https://github.com/clarin-eric/europeana-fulltext-aggregations) project on GitHub.

For more information and usage instructions, see the documentation of the above-mentioned project and
[compose_europeana_oai](https://gitlab.com/CLARIN-ERIC/compose_europeana_oai/).
